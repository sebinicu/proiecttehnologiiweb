const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

const app = express();
app.use(bodyParser.json());

const sequelize = new Sequelize('c9', 'sebinicu', '', {
   host: 'localhost',
   dialect: 'mysql',
   operatorsAliases: false,
   pool: {
        "max": 1,
        "min": 0,
        "idle": 20000,
        "acquire": 20000
    }
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


const Detalii_User = sequelize.define('detalii_user', {
   name: {
       type: Sequelize.STRING,
       allowNull: false
   }, 
   surname: {
        type: Sequelize.STRING,
        allowNull: false
   },
   username: {
       type: Sequelize.STRING,
       allowNull: false,
       primaryKey: true
   }, 
   password : {
       type: Sequelize.STRING,
       allowNull: false
   }
},
{
    timestamps: false
});


const Detalii_Masina = sequelize.define('detalii_masina',{
    nr_inmatriculare: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true
    },
    username: {
        type: Sequelize.STRING,
        allowNull: false
    }
},
{
    timestamps: false
});

const Parcare = sequelize.define('parcare',{
    id_parcare: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    lat: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    long: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    nr_max_locuri: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    nr_locuri_ocupate: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
},
{
    timestamps: false
});

const Loc_Parcare = sequelize.define('loc_parcare',{
    id_loc_parcare: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    id_parcare: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    nr_inmatriculare: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    }
},
{
    timestamps: false
});


sequelize.sync({force: true}).then(()=>{
    console.log('Databases create successfully')
})


app.post('/register', (req, res) =>{
    Detalii_User.create({
        name: req.body.name,
        surname: req.body.surname,
        username: req.body.username,
        password: req.body.password
    }).then((result) => {
        res.status(200).send("User created successfully");
    }, (err) => {
        res.status(500).send(err);
    })
})

app.get('/register', (req,res) =>{
    Detalii_User.findAll().then((result) =>{
        res.status(200).send(result);    
    });
});


app.post('/login', (req, res) => {
   Detalii_User.findOne({where:{username: req.body.username, password: req.body.password} }).then((result) => {
       res.status(200).send(result)
   })
});

app.post('/masini',(req,res) => {
    Detalii_Masina.create({
        nr_inmatriculare: req.body.nr_inmatriculare,
        username: req.body.username
    }).then((result) =>{
        res.status(200).send(result);    
    });
});

app.get('/masini',(req,res) => {
    Detalii_Masina.findAll().then((result) =>{
        res.status(200).send(result);    
    });
});


app.post('/locuri_parcare',(req,res) => {
    Loc_Parcare.create({
        id_loc_parcare: req.body.id_loc_parcare,
        id_parcare: req.body.id_parcare,
        nr_inmatriculare: req.body.nr_inmatriculare
    }).then((result) =>{
        res.status(200).send(result);    
    }, (err) => {
        res.status(500).send(err);
    });
});

app.get('/locuri_parcare',(req,res) => {
    Loc_Parcare.findAll().then((result) =>{
        res.status(200).send(result);    
    });
});

app.post('/parcari', (req,res) => {
    Parcare.create({
        id_parcare: req.body.id_parcare,
        lat: req.body.lat,
        long: req.body.long,
        nr_max_locuri: req.body.nr_max_locuri,
        nr_locuri_ocupate: req.body.nr_locuri_ocupate
    }).then((result) => {
       res.status(200).send(result); 
    }, (err) =>{
      res.status(500).send(err);  
    });
})


app.get('/parcari', (req,res) =>{
    Parcare.findAll().then((result) =>{
        res.status(200).send(result);    
    });
});


app.get('/parcari/:id', async (req, res) => {
	try{
		let parcare = await Parcare.findById(req.params.id)
		if (parcare){
			res.status(200).json(parcare)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/parcari/:id', async (req, res) => {
	try{
		let parcare = await Parcare.findById(req.params.id)
		if (parcare){
			await parcare.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/parcari/:id', async (req, res) => {
	try{
		let parcare = await Parcare.findById(req.params.id)
		if (parcare){
			await parcare.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})



app.listen(8080, ()=>{
    console.log('Server started on port 8080...');
})

